-- Config
local WIRELESS_CHAN
f = fs.open("channel", "r")
WIRELESS_CHAN = tonumber(f.readLine())
f.close()
print("My channel is "..WIRELESS_CHAN)

local QUARRY_SLOT = 1
local FUEL_SLOT   = 16
local LOADER_SLOT = 2
local CHAR_SLOT   = 3
local ITEM_SLOT   = 4
local JUNK_SLOT   = 5
local WAIT_TIME   = 870 -- 14.5 minutes
local TESS_TIME   = 35  -- pick up tesseract and wait for quarry to run out of energy

local modem = peripheral.wrap("right")

local args = {...}

os.loadAPI('scrpi')

function find_junk(digging_dir)
    return scrpi.find_empty_junk_slot(JUNK_SLOT, digging_dir)
end

function dig_if_detect()
    if turtle.detect() then
        turtle.select(JUNK_SLOT)
        turtle.dig()
        sleep(1)
    end
end

function digup_if_detect()
    if turtle.detectUp() then
        turtle.select(JUNK_SLOT)
        turtle.digUp()
        sleep(1)
    end
end

function select_and_empty(slot)
    if turtle.getItemCount(slot) > 0 then
        junk_slot = find_junk()
        turtle.select(slot)
        turtle.transferTo(junk_slot)
    end
    turtle.select(slot)
end

function go_forward()
    scrpi.go_x('forward', true)
end

function go_up()
    scrpi.go_x('up', true)
end

function go_down()
    scrpi.go_x('down', true)
end

function run_quarry()
    -- First we ask turtle 2 to pick up their tesseract.
    -- this is to avoid the turtle filling up with items 
    -- when it moves down, in case the quarry is not yet finished.
    modem.transmit(WIRELESS_CHAN, 1, "Get Tesseract!")
    sleep(TESS_TIME)

    go_down()

    -- Pick up charcoal chest
    turtle.turnLeft()
    select_and_empty(CHAR_SLOT)
    turtle.dig()
    sleep(1)
    turtle.turnRight()

    -- Pick up Quarry
    turtle.turnRight()
    select_and_empty(QUARRY_SLOT)
    turtle.dig()

    -- Pick up item chest
    select_and_empty(ITEM_SLOT)
    go_up()
    turtle.dig()
    sleep(1)
    turtle.down()
    turtle.turnLeft()

    -- Pick up chunk loader
    select_and_empty(LOADER_SLOT)
    turtle.dig()

    -- Move forward
    for i = 0,5 do
        dig_if_detect()
        go_forward()
    end

    -- Dig spot for Chunk Load turtle to refuel
    turtle.select(JUNK_SLOT)
    turtle.turnLeft()
    dig_if_detect()
    turtle.turnRight()
    dig_if_detect()
    go_forward()
    turtle.turnLeft()
    dig_if_detect()
    turtle.turnRight()
    dig_if_detect()
    go_forward()

    -- Dig a spot for the tesseract and energy turtle then move forward again
    turtle.turnRight()
    turtle.select(JUNK_SLOT)
    dig_if_detect()
    turtle.turnLeft()
    dig_if_detect()
    turtle.turnLeft()
    dig_if_detect()
    turtle.turnRight()
    go_forward()

    -- Dig spot for chunk loader
    turtle.select(JUNK_SLOT)
    dig_if_detect()

    -- Dig spot for fuel chest
    turtle.turnLeft()
    dig_if_detect()

    -- Dig spot for quarry
    turtle.turnRight()
    turtle.turnRight()
    dig_if_detect()

    -- Dig spot for item chest
    digup_if_detect()
    go_up()
    dig_if_detect()

    -- Place item chest
    scrpi.place_block(ITEM_SLOT, true)

    -- Remove junk
    for i = JUNK_SLOT,15 do
        turtle.select(i)
        turtle.drop()
    end

    -- Place quarry
    go_down()
    scrpi.place_block(QUARRY_SLOT, true)

    -- Place chunk loader
    turtle.turnLeft()
    scrpi.place_block(LOADER_SLOT, true)

    -- Place fuel chest and check level
    turtle.turnLeft()
    scrpi.place_block(CHAR_SLOT, true)
    if turtle.getFuelLevel() < 300 then
        print("Refuelling..")
        turtle.select(FUEL_SLOT)
        turtle.suck()
        turtle.refuel(64)
    end

    -- Move into quarrying position
    turtle.turnRight()
    go_up()

    -- Send signal to Energy Turtle to do it's thing.
    modem.transmit(WIRELESS_CHAN, 1, "Energise!")

    sleep(5)

    -- Send signal to Chunk Loader turtle
    modem.transmit(WIRELESS_CHAN, 1, "ChunkLoad!")
end

local WEB_CONTROL_STATE = "stop"-- State - "run", "cycle". "stop"
local WEB_CYCLES_REQU   = 0 -- Cycles requests from the web
local WEB_CYCLES_DONE   = 0

local cycling = false -- set to true when we are iterating through a cycle command

function web_sync()
    -- Check in to the web control interface every 10 seconds.
    -- This quarry can exist in the following states:
    -- * run - Running indefinately
    -- * cycle - Run For [x] cycles
    -- * stop - Stopped
    local response
    local refresh_cycles = 0
    local label = os.getComputerLabel()
    while true do
        response = http.post("http://localhost:1236/quarry_checkin",
            "label="..label..
            "&cyc_req="..WEB_CYCLES_REQU..
            "&cyc_done="..WEB_CYCLES_DONE
            )
        old_wcs = WEB_CONTROL_STATE
        WEB_CONTROL_STATE = response.readLine()
        refresh_cycles    = response.readLine()
        WEB_CYCLES_REQU   = tonumber(response.readLine())
        if old_wcs ~= WEB_CONTROL_STATE then
            print("Change in state: "..WEB_CONTROL_STATE..", cyc_requ: "..WEB_CYCLES_REQU)
        end

        if refresh_cycles == "true" then
            WEB_CYCLES_DONE = 0
            cycling = true
        end
        sleep(10)
    end
end

function quarry_manager()
    while true do
        if WEB_CONTROL_STATE == "stop" then
            cycling = false
            sleep(10)
        end
        if WEB_CONTROL_STATE == "run" then
            cycling = false
            run_quarry()
            sleep(WAIT_TIME)
        end
        if WEB_CONTROL_STATE == "cycle" and cycling == true then
            if WEB_CYCLES_DONE < WEB_CYCLES_REQU then
                WEB_CYCLES_DONE = WEB_CYCLES_DONE + 1
                print("Running Cycle "..WEB_CYCLES_DONE.." of "..WEB_CYCLES_REQU)
                run_quarry()
                sleep(WAIT_TIME)
            else
                cycling = false
                sleep(10)
            end
        end
    end
end

--iterations = args[1]
--resume     = args[2]

--if iterations == nil then iterations = 1 end

--if resume ~= nil then
    -- We have been asked to resume, meaning the quarry is running. wait for it to finish
    --print("Waiting for quarry to finish...")
    --sleep(WAIT_TIME)
--end

--for i = 1,iterations do
    --print("Running Quarry, iteration # "..i.." of "..iterations)
    --run_quarry()
    -- Wait a period of time for quarry to complete
    --sleep(WAIT_TIME)
--end
--scrpi.send_mail("QUARRY COMPLETE", "All done :)")

parallel.waitForAny(web_sync, quarry_manager)
