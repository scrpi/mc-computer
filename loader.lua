-- Config
local WIRELESS_CHAN
f = fs.open("channel", "r")
WIRELESS_CHAN = tonumber(f.readLine())
f.close()
print("My channel is "..WIRELESS_CHAN)

local LOADER_SLOT = 1
local FUEL_SLOT = 16
local CHAR_SLOT = 3

local modem = peripheral.wrap("right")
modem.open(WIRELESS_CHAN)

os.loadAPI('scrpi')

function go_forward()
    scrpi.go_x('forward')
end

function place_block(slot)
    scrpi.place_block(slot)
end

function run_loader()
    -- Pick up loader.
    turtle.select(LOADER_SLOT)
    turtle.dig()

    -- Move forward 
    for i = 0,8 do
        go_forward()
    end

    -- Place loader
    place_block(LOADER_SLOT)

    -- Check Fuel
    if turtle.getFuelLevel() < 300 then
        print("Refuelling..")
        turtle.select(FUEL_SLOT)
        turtle.turnLeft()
        go_forward()
        turtle.turnRight()
        go_forward()
        go_forward()
        turtle.suck()
        turtle.refuel(64)
        turtle.turnLeft()
        turtle.turnLeft()
        go_forward()
        go_forward()
        turtle.turnLeft()
        go_forward()
        turtle.turnLeft()
    end
end

while true do
    print ("Waiting for command..")
    local event, side, send_chan, reply_chan, msg, dist = os.pullEvent("modem_message")
    if msg == "ChunkLoad!" and send_chan == WIRELESS_CHAN then
        print("Doing some work..")
        run_loader()
    end
end
