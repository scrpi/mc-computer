function send_mail(subject, text)
    local response = http.post("http://localhost:1236/emailer", "subject="..subject.."&text="..text)
end

function stop_and_alert(msg)
    send_mail("QUARRY STUCK", msg)
    while true do
        sleep(60)
    end
end

function find_empty_junk_slot(start, digging_dir)
    local found = false
    local test_slot = start
    while found ~= true do
        print("Testing slot "..test_slot)
        if turtle.getItemCount(test_slot) == 0 then
            found = true
            return test_slot
        elseif digging_dir ~= nil then
            -- We are digging. Check if the type of block is the same in this slot and if there is room.
            if digging_dir == "forward" then
                compare = turtle.compare()
                if compare == true then
                    print("Compare True at slot "..test_slot)
                end
            end
            if digging_dir == "up" then
                compare = turtle.compareUp()
            end
            if digging_dir == "down" then
                compare = turtle.compareUp()
            end
            if compare == true and turtle.getItemCount(test_slot) < 64 then
                found = true
                return test_slot
            end
        end
        test_slot = test_slot + 1
        if test_slot > 15 then
            stop_and_alert("I'm full of junk!")
        end
    end
end

function go_x(dir, master)
    if master == nil then master = false end
    local continue = true
    local email_sent = false
    local tries = 0
    if dir == 'up' then
        f_dir = turtle.up
        f_attack = turtle.attackUp
        f_dig = turtle.digUp
    elseif dir == 'down' then
        f_dir = turtle.down
        f_attack = turtle.attackDown
        f_dig = turtle.digDown
    elseif dir == 'forward' then
        f_dir = turtle.forward
        f_attack = turtle.attack
        f_dig = turtle.dig
    else
        continue = false
    end
    while continue == true do
        tries = tries + 1
        if f_dir() then
            continue = false
        else
            if tries == 6 then
                send_mail("QUARRY STUCK", "I'm Stuck!!!")
                email_sent = true
            end
            if master == true then
                turtle.select(find_empty_junk_slot(6, dir))
                f_dig()
            end
            f_attack()
            sleep(1)
        end
    end
    if email_sent then
        send_mail("QUARRY RECOVERED", "I'm no longer stuck :)")
    end
end

function place_block(slot, master)
    if master == nil then master = false end
    local success = false
    local email_sent = false
    local tries = 0
    while success ~= true do
        tries = tries + 1
        turtle.select(slot)
        if turtle.place() then
            success = true
        else
            if tries == 6 then
                send_mail("QUARRY STUCK", "Can't place block!!!")
                email_sent = true
            end
            if master == true then
                turtle.select(find_empty_junk_slot(6, "forward"))
                turtle.dig()
            end
            turtle.attack()
            sleep(3)
        end
    end
    if email_sent then
        send_mail("QUARRY RECOVERED", "I placed the block OK now :)")
    end
end
