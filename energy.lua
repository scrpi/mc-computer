-- Config
local WIRELESS_CHAN
f = fs.open("channel", "r")
WIRELESS_CHAN = tonumber(f.readLine())
f.close()
print("My channel is "..WIRELESS_CHAN)

local TESSERACT_SLOT = 1
local CONDUIT_SLOT = 2
local FUEL_SLOT = 16
local CHAR_SLOT = 3

local modem = peripheral.wrap("right")
modem.open(WIRELESS_CHAN)

os.loadAPI('scrpi')

function go_forward()
    scrpi.go_x('forward')
end

function go_up()
    scrpi.go_x('up')
end

function go_down()
    scrpi.go_x('down')
end

function place_block(slot)
    scrpi.place_block(slot)
end

function run_energise()
    -- Move forward 8
    for i = 0,8 do
        go_forward()
    end

    -- Place tesseract
    turtle.turnRight()
    go_up()
    place_block(TESSERACT_SLOT)
    go_down()
    turtle.turnLeft()

    -- Check fuel
    if turtle.getFuelLevel() < 300 then
        print("Refuelling..")
        turtle.select(FUEL_SLOT)
        turtle.turnLeft()
        go_forward()
        turtle.turnRight()
        turtle.suck()
        turtle.refuel(64)
        turtle.turnRight()
        go_forward()
        turtle.turnLeft()
    end
end

function get_tesseract()
    -- Wrench tesseract
    turtle.turnRight()
    go_up()
    turtle.select(TESSERACT_SLOT)
    turtle.dig()
    go_down()
    turtle.turnLeft()
end

while true do
    print ("Waiting for command..")
    local event, side, send_chan, reply_chan, msg, dist = os.pullEvent("modem_message")
    if msg == "Energise!" and send_chan == WIRELESS_CHAN then
        run_energise()
    end
    if msg == "Get Tesseract!" and send_chan == WIRELESS_CHAN then
        get_tesseract()
    end
end
